# Planificacio del projecte
## Marc Serra Hidalgo
### GUI-Tool For Linux System Administration

Temps total: 8 setmanes (4 extraoficials, 4 oficials)

Planificacio de les 4 setmanes extraoficials:
- Creacio del projecte de DJANGO, l'entorn de treball i l'entorn virtual.
- Creacio dels scripts de instal·lació de la aplicació.
- Creacio dels scripts de posada en marxa rapida de la aplicació.
- Creacio del disseny web i del model de plantilles
- Començada l’eina: crear usuaris (practicament acabada).
- Començada l’eina: crear rutines de backups (practicament acabada).

Planificacio de les 4 setmanes oficials:
- SETMANA 1:
 	- Incorporar a classe la feina feta a casa amb anterioritat
 	- Acabar i polir el codi i el funcionament de les dues primeres eines de l'aplicacio que ja han estat començades.
 	- Generar documentació
- SETMANA 2: 
	- Crear la primera eina complexa (Dockers)
	- Potser afegir una eina simple mes
	- Generar documentació
- SETMANA 3: 
	- Crear la segona eina complexa (BBDD)
	- Potser afegir una eina simple mes
	- Crear la BBDD interna per utilitzar el buscador de Django
	- Generar documentació
- SETMANA 4:
	- Fer refractor de tot el codi (comentar les parts que faltin, eliminar codi innecessari, fer el codi mes entenedor, etc.) inclosos dels scripts de instalacio i posada en funcionament.
	- Acabar documentació
	- Preparar la presentació de diapositives
	- Preparar la presentació oral
	- Asegurar que el metode de desplegament funciona al PC de l'aula del profe.