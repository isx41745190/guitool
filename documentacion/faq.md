# F.A.Q

## GUI-Tool For Linux System Adminsitration

### ¿Que es GUI Tool for Linux System Administration?
Es una herramienta gràfica que permite a los administradores de sistemas Fedora realizar algunas de las acciones de administración local de manera sencilla, rápida y ágil gracias a una plataforma web adaptada para este fin. 

### ¿Con que tecnologías se ha desarrollado?
- *Django*
- *Python*
- *Bash*
- *Entornos Virtuales*

### ¿Que herramientas nos aporta?
- Crear usuarios
- Crear rutinas de copias de seguridad
- Crear contenedores Docker
- Crear bases de datos Postgres

### ¿Cuales son los requsitos del sistema?
La instalación ya prevé algunos requisitos que pueden faltar en tu sistema y los instala. Aun así, se necesitan de ciertos requerimientos previos:
- *Python 2*
- *Terminal* (o similar)

### ¿Puedo utilizar la herrmienta sin ser root?
No. Actualmente la plataforma es solo para adminsitradores de sistema, por lo que para el uso de las herramientas se necesitan de los privilegios del todopoderoso ROOT.

### ¿Puedo participar en el proyecto?
Claro! Todo desarrollador que quiera participar en el proyecto es bienvenido.

### ¿Como participar en el proyecto?
Para poder participar del proyecto simplemente tienes que crear un fork del repositorio y ya puedes empezar a proponer codigo mediante pull-requests! 

### ¿Cuanto espacio de almacenamiento necesito?
Depende de los requerimientos que tu sistema aún no tenga satisfechos. Aun así, todo el proyecto puede necesitar un máximo de 150 MB.

### ¿Como instalo la aplicación?
Entra en la carpeta *for_developing* del repositorio, descargate el fichero *install_guitool.sh* y ejecutalo como root en tu máquina.

### Estoy decidido a instalarme la herramienta. ¿Que cambios hará su instalación en mi sistema?
- Instalará los siguiente paquetes: *git*, *python2-virtualenv*, *python3*, *google-chrome-stable.x86_64*, *screen*

- Creara las siguientes carpetas: `/etc/guitool`, `~/Backups_guitool`.

- Creara los siguientes alias al usuario root: *workon*, *guitool_start*, *project_dir*, *guitool_developing*, *guitool_stop*

### ¿Funciona con cualquier version de Linux?
No. Actualmente la plataforma solo se se compromete a estar 100% disponible para el sistema operativo Fedora 24.
