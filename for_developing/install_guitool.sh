#######################################
# Define constants and functions
#######################################
OK=0
ERR_INSTALL_GIT=1
ERR_INSTALL_VEN=2
ERR_INSTALL_PY3=3
ERR_INSTALL_CHR=4
ERR_CD=5
ERR_CLONE_PROJ=6
ERR_CREAT_VEN=7
ERR_WRITE_BRC=8
ERR_RM_GIT=9
ERR_RM_SCR=9

function check_correct_or_exit {
	if [ $? -eq 0 ]
	then
		echo "DONE."
	else
		echo "ERROR: [$1] Exiting..."
		exit $1
	fi
}
#######################################
# Install basic dependencies
#######################################
# Git because need to clone the project
dnf -y install git > /dev/null && echo "Install GIT OK"
check_correct_or_exit $ERR_INSTALL_GIT
# Virtual env because django runs in venv
dnf -y install python2-virtualenv > /dev/null && echo "Install Python2 Venv OK"
check_correct_or_exit $ERR_INSTALL_VEN
# Python 3 because created venv runs with python 3
dnf -y install python3 > /dev/null && echo "Install Python3 OK"
check_correct_or_exit $ERR_INSTALL_PY3
# Firefox because is needed
# dnf -y install firefox > /dev/null && echo "Install Firefox OK"
cat << EOF > /etc/yum.repos.d/google-chrome.repo
[google-chrome]
name=google-chrome
baseurl=http://dl.google.com/linux/chrome/rpm/stable/x86_64
enabled=1
gpgcheck=1
gpgkey=https://dl.google.com/linux/linux_signing_key.pub
EOF
dnf -y install google-chrome-stable.x86_64 > /dev/null && echo "Install Chrome OK"
check_correct_or_exit $ERR_INSTALL_CHR
# Screen because is needed for run server in background
dnf -y install screen > /dev/null && echo "Install Screen OK"
check_correct_or_exit $ERR_INSTALL_SCR

#######################################
# Changes in system
#######################################
# Go to /etc directory and clone the project
cd /etc > /dev/null && echo "Workdir /etc/ OK"
check_correct_or_exit $ERR_CD
git clone https://gitlab.com/isx41745190/guitool.git > /dev/null && echo "Git clone OK"
check_correct_or_exit $ERR_CLONE_PROJ
# Once project was cloned, go in and create venv with python3
cd guitool > /dev/null && echo "Workdir GUIT OK"
check_correct_or_exit $ERR_CD
virtualenv --python=/usr/bin/python3.5 venv/ > /dev/null && echo "Venv created OK"
check_correct_or_exit $ERR_CREAT_VEN
source venv/bin/activate
pip install -r requirements.txt && echo "Installed venv requirements OK"
deactivate
# Create aliases in .bashrc
echo "
# Edited to run GUITOOL
# For client
alias workon='source /etc/guitool/activate_env.sh'
alias guitool_start='workon ; /etc/guitool/start_app.sh ; deactivate'
# For developing
alias project_dir='cd /etc/guitool/GUIT_LinuxAdministration'
alias guitool_developing='workon ; project_dir; /etc/guitool/for_developing/start_developing.sh'
alias guitool_stop='pkill -f DjangoServer'
" >> ~/.bashrc && echo "Updates root bashrc OK"
check_correct_or_exit $ERR_WRITE_BRC
# Remove git repository files
rm -rf /etc/guitool/.git && echo "Removed .git OK"
check_correct_or_exit $ERR_RM_GIT
# Create directory for the backup tool
mkdir ~/Backups_guitool
#######################################
# Finish
#######################################
echo "----- FINISH -----"
echo "You need to restart terminal. Then, run guitool_start!"
exit $OK
