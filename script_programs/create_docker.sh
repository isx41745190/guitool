#! /bin/bash
so_base=$1
name=$2
hostnamee=$3
network=$4
workdir=$5
to_install=$6
privileged=$7

docker pull $so_base
if [ $? -ne 0 ]
	then
	exit 3
fi

# if user not specify work directory
if [ "$workdir" = 'None' ]
	then
	workdir="/opt/docker"
fi

# if user dont want to install any paquets
if [ "$to_install" = 'None' ]
	then
	to_install=""
fi

# delete previous images
docker rmi -f dockerguitool

# Build dockerfile
cat > /etc/guitool/tmp/Dockerfile <<- EOF
FROM $so_base
RUN mkdir -p $workdir
COPY . $workdir
WORKDIR $workdir
$to_install
RUN rm -rf $workdir/Dockerfile
CMD ["/bin/bash"]
EOF

# change to context dir
cd /etc/guitool/tmp

# build image
docker build -t dockerguitool .
if [ $? -ne 0 ]
	then
	exit 5
fi
cd -

# Create docker example whit same options for check error of name or hostname- If result ok, then i will can create docker
docker run --rm=true --name $name -h $hostnamee --privileged=$privileged --net $network -it dockerguitool ls
if [ $? -ne 0 ]
	then
	exit 2
fi
# Run correct docker
dbus-launch gnome-terminal -e "docker run --rm=true --name $name -h $hostnamee --privileged=$privileged --net $network -it dockerguitool /bin/bash" & disown
if [ $? -ne 0 ]
	then
	# error cualquiera
	exit 4 
fi

rm -rf /etc/guitool/tmp
if [ $? -ne 0 ]
	then
	# error cualquiera
	exit 4 
fi

exit 0