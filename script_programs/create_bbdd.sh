#! /bin/bash
user=$1
password=$2
export PGPASSWORD=$password
psql -U $user --no-password -f /tmp/script_gui.sql template1 &> /tmp/guitool_bbdd_logs.log

errors=$(grep "FATAL\|ERROR" /tmp/guitool_bbdd_logs.log)
# Si grep no troba res, ok. No hi ha errors i surt amb 0.
if [ $? -eq 1 ]
	then
	echo "esto_ha_ido_bien"
	exit 0
else # Si grep ha trobat algo, mostreu perque agafare tota sa sortida i surt amb nombre error amb errors no previamnet controlats.
	echo "$errors"
	exit 500
fi