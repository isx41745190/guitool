#!/bin/bash
# Get all arguments
directorio_origen=$1
crear_ahora=$2
opcion_timing=$3
directorio_destino="/root/Backups_guitool/"
file_name=backup-$(date +%-Y%-m%-d)-$(date +%-T).tgz
# Check if FROM dir exists
if [ ! -d "$directorio_origen" ]
	then
	exit 1
fi
# Check if TO dir exists
if [ ! -d "$directorio_destino" ]
	then
	exit 2
fi
# If option crear_ahora is set.. create backup now
if [ "$crear_ahora" = "yes" ]
	then
	/etc/guitool/script_programs/run_backup.sh $directorio_origen $directorio_destino $file_name
	# tar --create --gzip --file=$directorio_destino$file_name $directorio_origen 2> /dev/null
	# Check if is done
	if [ $? -ne 0 ]
		then
		exit 3
	fi
fi
# Translate timing opion user selected
if [ "$opcion_timing" = "dia" ]
	then
	timing="0 8 * * *"

elif [ "$opcion_timing" = "semana" ]
	then
	timing="0 0 * * 0"

elif [ "$opcion_timing" = "mes" ]
	then
	timing="0 0 1 * *"
fi
# Check if already exists the same cron. If grep's return 0, exists!
crontab -l | grep -F "$timing /etc/guitool/script_programs/run_backup.sh $directorio_origen"
if [ $? -eq 0 ]
	then
	exit 4
fi
# Create crontab
crontab -l > mycron
echo "$timing /etc/guitool/script_programs/run_backup.sh $directorio_origen $directorio_destino $file_name" >> mycron
crontab mycron
rm mycron
# Exit program DONE
exit 0
